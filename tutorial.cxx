#include <iostream>
#include <string>
#include <cmath>

#include "TutorialConfig.h"

#ifdef USE_MYMATH
#include "MathFunctions.h"
#endif

using namespace std;

int main(int argc, char *argv[])
{
	if (argc < 2) {
		// report version
		cout << argv[0] << " Version "
			<< Tutorial_VERSION_MAJOR << "."
			<< Tutorial_VERSION_MINOR << endl;
		cout << "Usage: " << argv[0] << " number" << endl;
		return 1;
	}

	double number = stod(argv[1]);
#ifdef USE_MYMATH
	mysqrt(number);
#else
	cout << "The square root of " << number << " is ";
	cout << sqrt(number) << endl;
#endif

	return 0;
}
